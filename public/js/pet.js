const btnSearch = document.getElementById('search')
const popupCloseBtn = document.getElementById("close-popup");
const mealInfoEl = document.getElementById("meal-info");
const mealPopup = document.getElementById("meal-popup");

firebase.initializeApp({
    apiKey: "AIzaSyCIbwGvE6pD3nq5V6VOTvpfRhCGnymGRoo",
    authDomain: "mobileweb-b370e.firebaseapp.com",
    databaseURL: "https://mobileweb-b370e.firebaseio.com",
    projectId: "mobileweb-b370e",
    storageBucket: "mobileweb-b370e.appspot.com",
    messagingSenderId: "34712387469",
    appId: "1:34712387469:web:11d3bf23eb3026b28b47c4",
    measurementId: "G-XNHQRMJCPH"
})
var database = firebase.database();

getRandoMeal()
async function getRandoMeal() {
    // funcion para obtener todos los datos
    let arreglo = []
    database.ref('celulares').once('value').then(datos => {
        datos.forEach(nodo => {
            arreglo.push(nodo);
        })
        publications(arreglo)
    }).catch(q => {
        console.log(q)
    })
}

function publications(arreglo) {
    arreglo.forEach(element => {
        createPublications(element)
    });
}

function createPublications(arreglo) {
    // metodo para crear las publicaciones
    const mealsList = document.getElementById('meals')
    const publication = document.createElement('div')
    publication.innerHTML = `
        <div class="meal_header">
            <img src="${arreglo.val().img}"/>
            <alt="${arreglo.val().img}">
        </div>
        <div class="meal_body">
            <div class="details">
            <h4>Numero de serie = ${arreglo.val().model}</h4>
            <h4>Descripcion = ${arreglo.val().descrip}</h4>
            <h4>Tamaño = ${arreglo.val().tamano}</h4>
            <h4>Sistema operativo = ${arreglo.val().os}</h4>
            </div>
            <button class="fav_btn">
                <i class="fas fa-trash-alt"></i>
            </button>
        `
    const btnProd = publication.querySelector('.meal_body .fav_btn')
    const headerPhoto = publication.querySelector('.meal_header')

    btnProd.addEventListener('click', () => {
        //eliminar publicacion
        database.ref('celulares/' + arreglo.key).remove().then(p => {
            alert('registro eliminado')
        });

    })

    headerPhoto.addEventListener('click', () => {
        // mostrar informacion para editar
        showEdit(arreglo)
    })

    mealsList.appendChild(publication)
}


function showEdit(arreglo) {

    //editar informacion
    mealInfoEl.innerHTML = ""
    const editpublic = document.createElement('div')

    editpublic.innerHTML = ` 
    <img src="${arreglo.val().img}"/>
    <h1>${arreglo.val().model}</h1>
    <label>Descripcion</label>
    <input type="text" class="descrip" value="${arreglo.val().descrip}"/>
    <label>Sistema operativo</label>
    <input type="text" class="os" value="${arreglo.val().os}"/>
    <label>Tamano</label>
    <input type="text" class="tamano" value="${arreglo.val().tamano}"/>
    <button class="update_info">
                <i class="fas fa-paper-plane"></i>
    </button>
    `
    const btnupdate = editpublic.querySelector('.update_info')
    const descrip = editpublic.querySelector('.descrip')
    const os = editpublic.querySelector('.os')
    const tamano = editpublic.querySelector('.tamano')

    btnupdate.addEventListener('click', () => {
        const model = arreglo.key
        database.ref('celulares/' + model).set({
            descrip: descrip.value,
            model: model,
            os: os.value,
            img: "https://images.pexels.com/photos/607812/pexels-photo-607812.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
            tamano: tamano.value
        })
        mealPopup.classList.add("hidden");
    })

    mealInfoEl.appendChild(editpublic)
    mealPopup.classList.remove("hidden")
}

popupCloseBtn.addEventListener("click", () => {
    mealPopup.classList.add("hidden");
});

btnSearch.addEventListener('click', function () {
    // guardar publication
    var numero = document.getElementById('numero')
    var descrip = document.getElementById('descrip')
    var camara = document.getElementById('camara')
    var os = document.getElementById('os')
    var tamano = document.getElementById('tamano')
    database.ref('celulares/' + numero.value).set({
        descrip: descrip.value,
        camara: camara.value,
        model: numero.value,
        os: os.value,
        img: "https://images.pexels.com/photos/607812/pexels-photo-607812.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
        tamano: tamano.value
    })
    numero.innerHTML = ""
    descrip.innerHTML = ""
    camara.innerHTML = ""
    os.innerHTML = ""
    tamano.innerHTML = ""
    alert('Producto creado')
})